import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { httpClient } from '../util/Requests';
import { displayAlert } from '../util/Util';

export const useSession = () => {
    const navigate = useNavigate();
    useEffect(async () => {
        if (localStorage.getItem('token')) {
            httpClient.addHeader('Authorization', `Bearer ${JSON.parse(localStorage.getItem('token'))}`)
            let response = await (await httpClient.post('/auth/verify-session')).json();
            if (!response.status) {
                await displayAlert('Error','Sesión Expirada','error');
                navigate('/');
            }
        }else{
            await displayAlert('Error','Sesión Expirada','error');
            navigate('/');
        }
    }, []);
}
