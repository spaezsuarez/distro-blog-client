import React, { useState, useEffect } from 'react';
import { httpClient } from '../../util/Requests';
import { ReviewItem } from '../../components/ListReviews/ReviewItem';
import { useSession } from '../../hooks/useSession';
import styles from './Reviews.module.css';

import List from '@mui/material/List';

export const Reviews = () => {
    
    useSession();
    
    const [listDistroReviews, setListDistroReviews] = useState([]);

    useEffect(async () => {
        httpClient.addHeader('Authorization', `Bearer ${JSON.parse(localStorage.getItem('token'))}`);
        const response = await (await httpClient.get(`/distributions/all`)).json();
        if(response.status){
            setListDistroReviews(response.data);
        }
    },[]);
    

    return (
        <div className={styles.general_container}>  
            <h1>Mira aqui las reseñas de varios usuarios sobre distintas distribuciones</h1>
            <List className={styles.reviews_container}>
                {
                    listDistroReviews.map((element) => {
                        return <ReviewItem key={element.id_distribution} {...element} />
                    })
                }

            </List>
        </div>
    )
}