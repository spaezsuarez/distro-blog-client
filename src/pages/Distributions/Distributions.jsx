import React, { useEffect, useState } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import Alert from '@mui/material/Alert';
import { CardDistribution } from '../../components/CardDistribution/CardDistribution';
import { ListLayout } from '../../components/ListLayout/ListLayout';
import { httpClient } from '../../util/Requests';
import { useSession } from '../../hooks/useSession';
import styles from './Distributions.module.css';
import Swal from 'sweetalert2';

export const Distributions = () => {
    useSession();

    const [distributions, setDistributions] = useState([]);
    const [dataLoaded, setDataLoaded] = useState(false);
    const [showAlert, setShowAlert] = useState(false);

    useEffect(async () => {
        httpClient.addHeader('Authorization', `Bearer ${JSON.parse(localStorage.getItem('token'))}`);
        const response = await (await httpClient.get(`/distributions/all`)).json();
        setDataLoaded(response.state);
        setTimeout(() => {
            setDistributions(response.data);
            setDataLoaded(true);
        }, 2000);
    }, []);

    const addList = async ({ id_distribution }) => {
        const bodyData = {
            'id_distribution':id_distribution,
            'opinion':''
        };
        httpClient.addHeader('Authorization', `Bearer ${JSON.parse(localStorage.getItem('token'))}`)
        const response = await (await httpClient.post('/user-distributions/add-favorite',bodyData)).json();
        if(response.status){
            setShowAlert(true);
            setTimeout(() => {
                setShowAlert(false);
            },1000);
        }else{
            await Swal.fire(
                'Error',
                'Ha ocurrido un error intentelo más tarde',
                'error'
            );
        }
    }

    if (dataLoaded) {
        return (
            <>
                {(showAlert) ?
                    <Alert className={styles.alert} severity={'success'}>Elemento Agregado</Alert> : <></>
                }
                <div>
                    {(distributions.length === 0) ? <p>No hay Datos disponible</p>
                        : <ListLayout>
                            {distributions.map((distribution) => {
                                return (
                                    <CardDistribution 
                                        key={distribution.id_distribution}
                                        distribution={distribution}
                                        addList={addList} 
                                    />
                                )
                            })}
                        </ListLayout>
                    }
                </div>
            </>
        )
    } else {
        return <div className={styles.spinner_container}>
            <Spinner animation="border" variant="danger" />
        </div>
    }


}