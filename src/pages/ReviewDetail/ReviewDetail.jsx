import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { httpClient } from '../../util/Requests';
import ListGroup from 'react-bootstrap/ListGroup';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Image from 'react-bootstrap/Image';

export const ReviewDetail = () => {
    const { id_distribution } = useParams();

    const [opinions, setOpinions] = useState([]);
    const [distributionData, setDistributionData] = useState({});

    const getOpinionsData = async () => {
        const responseOpinions = await (await httpClient.get(`/user-distributions/distribution-opinions?id_distribution=${id_distribution}`)).json();
        if (responseOpinions.status) {
            setOpinions(responseOpinions.data.opinions);
            console.log(opinions);
        }
    }

    const getDistributionData = async () => {
        const response = await
            (await httpClient.get(`/distributions/?id_distribution=${id_distribution}`)).json();
        if (response.status) {
            setDistributionData(response.data);
        }
        return response.data.id_distribution;
    }

    useEffect(async () => {
        httpClient.addHeader('Authorization', `Bearer ${JSON.parse(localStorage.getItem('token'))}`);
        await getOpinionsData();
        await getDistributionData();
    }, []);

    return (
        <>
            <Jumbotron>
                <div className="d-flex justify-content-center">
                    <h1>Foro de opiniones sobre {distributionData.name}</h1>
                </div>
            </Jumbotron>
            <hr></hr>
            <ListGroup as="ol" numbered>
                {
                    opinions.map((element) => {
                        if (element.opinion !== '') {
                            return <ListGroup.Item
                                variant="primary"
                                key={element.id_user}
                                as="li"
                                className="d-flex justify-content-between align-items-start"
                            >
                                <div className="ms-2 me-auto">
                                    <div className="fw-bold"> <b>{element.user}</b></div>
                                    {element.opinion}
                                </div>
                            </ListGroup.Item>
                        }
                    })
                }
            </ListGroup>
        </>
    )
}