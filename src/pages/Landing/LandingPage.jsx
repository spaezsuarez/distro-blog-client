import React from 'react';
import { Item } from '../../components/Item/Item';
import styles from './Landing.module.css';
import logoDistros from '../../../assets/logo-distros.png';
import reviewImage from '../../../assets/review.jpg';

export const LandingPage = () => {
    return <div>
        <div className="position-reltive overflow-hidden p-3 p-md-5 m-md-3 text-center">
            <h2>Distro Blog</h2>
            <hr />
            <p className={styles.ptext}>
                Esta es una pagina hecha como proyecto para el grupo de trabajo e investigación GLUD 
                (Grupo GNU Linux de la Universidad Distrital) a continuación se muestran algunas funcionalidades basicas que se tienen
                para esta versión inicial, se puede consultar màs información sobre la api en que consume este proyecto
                <a href="https://gitlab.com/spaezsuarez/distro-api-blog" target='_blank' className={styles.a_home} > <b>Aqui</b></a>.
            </p>
        </div>

        <div className="position-reltive overflow-hidden p-3 p-md-5 m-md-3 text-center">
            <div className="row">
                <div className="col-6">
                    <Item title="Distribuciones" alt="Distribuciones"
                        image={logoDistros}
                        text="Podras ver información basica sobre algunas distribuciones linux y si asi lo deseas agregarlas a tú lista y opinar sobre ellas"
                        route="/distributions"
                    />
                </div>
                <div className="col-6">
                    <Item title="Reseñas" alt="Reseñas"
                        image={reviewImage}
                        text="Puedes revisar las reseñas hechas por varios usuarios sobre una distribución en particular"
                        route="/reviews"
                    />
                </div>
            </div>
        </div>
    </div>
}