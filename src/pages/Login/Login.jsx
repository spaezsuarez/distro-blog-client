import React,{ useEffect } from 'react';
import styles from './Login.module.css';
import Button from 'react-bootstrap/Button';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Input from '@mui/material/Input';

import { useNavigate,Link } from 'react-router-dom';
import { httpClient } from '../../util/Requests';

export const Login = () => {
    const navigate = useNavigate();

    const authenticationData = {
        'email': '',
        'password_blog': ''
    }

    const setProperty = (property, value) => {
        authenticationData[property] = value;
    }

    const handleSubmit = async (e) => {
        let response = await (await httpClient.post('/auth/login',authenticationData)).json();
        console.log(response);
        if(response.status){
            localStorage.setItem('token',JSON.stringify(response.data.token));
            navigate('/distributions');
        }
    }

    return (
        <div className={styles.login_container}>
            <div className={styles.form_container}>
                <h2 className={styles.form_container_title}>
                    Inicia Sesión
                </h2>
                <hr style={{'opacity':'0'}} />
                <div className={styles.box_form}>
                    <FormControl>
                        <InputLabel htmlFor="mail-input">Correo</InputLabel>
                        <Input
                            id="mail-input"
                            onChange={(e) => { setProperty('email', e.target.value) }}
                            aria-describedby="input for mail"
                        />
                    </FormControl>
                    <hr style={{ opacity: "0" }}></hr>
                    <FormControl>
                        <InputLabel htmlFor="password-input">Contraseña</InputLabel>
                        <Input
                            id="password-input"
                            type='password'
                            onChange={(e) => { setProperty('password_blog', e.target.value) }}
                        />
                    </FormControl>
                    <hr style={{ opacity: "0" }}></hr>
                    <Button onClick={handleSubmit} variant="success">Iniciar Sesión</Button>
                </div>
            </div>
            <div className={styles.background}>
                <h2 className={styles.background_title}>
                    ¿Eres Nuevo?
                </h2>
                <hr style={{'opacity':'0'}} />
                <p style={{'color':'white','fontSize':'17px'}}>
                    Registrate y comparte sobre tus experiencias con distribuciones linux
                </p>
                <hr style={{'opacity':'0'}} />
                <Link className={styles.background_link} to='/sign-up'>Registrarte</Link>
            </div>
        </div>
    )
}