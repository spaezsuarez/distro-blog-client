import React,{ useState } from 'react';
import styles from './SignUp.module.css';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Input from '@mui/material/Input';
import Alert from '@mui/material/Alert';

import Swal from 'sweetalert2';
import { httpClient } from '../../util/Requests';
import { useNavigate } from 'react-router-dom';

export const SignUp = () => {

    const navigate = useNavigate();
    const [showAlert,setShowAlert] = useState(false);
    const [isError,setIsError] = useState(false);
    const [stateMessage,setStateMessage] = useState('');

    let confirmPassword = '';
    const signUpData = {
        'username': '',
        'email': '',
        'password_blog': ''
    }

    const setProperty = (property, value) => {
        signUpData[property] = value;
    }

    const showStatusSignUp = (response) => {
        setIsError(response.state);
        setStateMessage(response.message);
        setShowAlert(true);
        setTimeout(() => {
            setShowAlert(false)
            navigate('/');
        },5000);
    }

    const handleSubmit = async (e) => {
        if(signUpData['password_blog'] === confirmPassword){
            httpClient.post('/auth/sign-up',signUpData).then(async (response) => {
                showStatusSignUp(await response.json());
            }).catch(() => {
                showStatusSignUp({state:false,message:'Error registrando el usuario'});
            });
        }else{
            await Swal.fire(
                'Error',
                'Asegurese que las contraseñas coinciden',
                'error'
              )
        }
    }

    return (
        <div className={styles.signup_wrapper}>
            
            <div className={`${styles.signup_form} animate__animated animate__fadeIn `}>
            <h3>Registro de usuario</h3>
                {
                    (showAlert)?
                    <Alert className={styles.alert} severity={(!isError)?'success':'error'}>{stateMessage}</Alert>:<></>
                }
                <FormControl className={styles.form_control}>
                    <InputLabel htmlFor="username-input">Nombre de usuario</InputLabel>
                    <Input
                        id="username-input"
                        onChange={(e) => { setProperty('username', e.target.value) }}
                        aria-describedby="input for username"
                    />
                </FormControl>
                <FormControl className={styles.form_control}>
                    <InputLabel htmlFor="mail-input">Correo</InputLabel>
                    <Input
                        id="mail-input"
                        onChange={(e) => { setProperty('email', e.target.value) }}
                        aria-describedby="input for mail"
                    />
                </FormControl>
                <FormControl className={styles.form_control}>
                    <InputLabel htmlFor="password-input">Contraseña</InputLabel>
                    <Input
                        id="password-input"
                        type='password'
                        onChange={(e) => { setProperty('password_blog', e.target.value) }}
                    />
                </FormControl>
                <FormControl className={styles.form_control}>
                    <InputLabel htmlFor="second-password-input">Confirmar contraseña</InputLabel>
                    <Input
                        id="second-password-input"
                        type='password'
                        onChange={(e) => { confirmPassword = e.target.value }}
                    />
                </FormControl>
                <br />
                <Button onClick={handleSubmit} variant="contained">Registrarme</Button>
            </div>
        </div>
    )
}