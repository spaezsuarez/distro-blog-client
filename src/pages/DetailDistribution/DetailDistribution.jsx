import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSession } from '../../hooks/useSession';
import { httpClient } from '../../util/Requests';
import { displayAlert } from '../../util/Util';

import Figure from 'react-bootstrap/Figure';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import styles from './DetailDistribution.module.css';
import SaveIcon from '@mui/icons-material/Save';
import IconButton from '@mui/material/IconButton';

export const DetailDistribution = () => {

    useSession();

    const { id_distribution } = useParams();
    const [ distroDetail,setDistroDetail ] = useState({});
    const [ opinionUser,setOpinionUser ] = useState('');
    const [ opinionEnabled,setOpinionEnabled ] = useState(false);

    const getDataDetail = async () => {
        const response = await 
            (await httpClient.get(`/distributions/?id_distribution=${id_distribution}`)).json();
        if (response.status) {
            setDistroDetail(response.data);
        }
        return response.data.id_distribution;
    }

    const getOpinion = async (id_distribution) => {
        const response = await (await httpClient.get(`/user-distributions/opinion/?id_distribution=${id_distribution}`)).json();
        setOpinionEnabled(response.data.isEnabled);
        if (response.status) {
            setOpinionUser(response.data.opinion);
        }
    }

    const updateOpinion = async() =>  {
        if(opinionEnabled){
            const bodyData = {
                'id_distribution':distroDetail.id_distribution,
                'opinion':opinionUser
            }
            const response = await (await httpClient.put('/user-distributions/opinion',bodyData)).json();
            if(response.status){
                await displayAlert('Info','Opinión actualizada correctamente','success');
            }
        }
    }

    useEffect(async () => {
        httpClient.addHeader('Authorization', `Bearer ${JSON.parse(localStorage.getItem('token'))}`);
        const id_distribution = await getDataDetail();
        await getOpinion(id_distribution);
    }, []);

    return (
        <>
            <div className={styles.detail_container}>
                <div className={styles.logo_background}>
                    <h1>{distroDetail.name}</h1>
                    <hr style={{ 'opacity': '0' }}></hr>
                    <Figure className={styles.figure}>
                        <Figure.Image
                            alt={`${distroDetail.name}logo`}
                            src={distroDetail.logo}
                            rounded
                        />
                    </Figure>
                </div>
                <div className={styles.form_opinion}>
                    <h2>Información basica</h2>
                    <div className={styles.data_container}>
                        <p className={styles.form_opinion_description}><b>Descripción:</b> {distroDetail.description}</p>
                        <ol className="list-group">
                            <li className="list-group-item">Arquitectura: {distroDetail.architecture}</li>
                        </ol>
                    </div>
                    <div className={styles.input_containers}>
                        <h4>Sube tu opinión</h4>
                        <TextareaAutosize className={styles.form_opinion_input}
                            minRows={5}
                            placeholder="¿Qué opinas de la distribución?"
                            value={opinionUser}
                            onChange={(e) => setOpinionUser(e.target.value)}
                            disabled={!opinionEnabled}
                        />
                        <IconButton  onClick={updateOpinion} color="success"  size="large">
                            <SaveIcon /> 
                        </IconButton>
                    </div>
                </div>
            </div>
        </>
    )
}