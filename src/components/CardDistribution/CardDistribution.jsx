import React from 'react';
import { useNavigate } from 'react-router-dom';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import styles from './CardDistribution.module.css';

export const CardDistribution = ({ distribution,addList }) => {
    const { name,summary,official_url,logo } = distribution;
    const navigate = useNavigate();

    const viewDetail = () => {
        navigate(`/detail/${distribution.id_distribution}`);
    }

    return (
        <Card className={styles.card} style={{ width: '18rem' }}>
            <Card.Img  className={styles.card_img} variant="top" src={logo} />
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text>
                    {summary}
                </Card.Text>
                <Card.Link href={official_url} target='__blank' >Pagina Oficial</Card.Link>
                <hr style={{'opacity':'0'}}></hr>
                <div className={styles.btn_container}>
                    <Button variant="success" onClick={viewDetail} >Detalles</Button>
                    <Button variant="warning" onClick={() => addList(distribution)}>Agregar a mi lista</Button>
                </div>
            </Card.Body>
        </Card>
    )
}