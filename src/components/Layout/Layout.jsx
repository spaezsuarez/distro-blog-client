import React from "react";
import styles from './Layout.module.css';
import { NavBar } from '../NavBar/Navbar';

export const Layout = ({ children }) => {
  return (
    <div className={styles.general_container}>
      <NavBar />
      <main>
        {children}
      </main>
    </div>
  );
};