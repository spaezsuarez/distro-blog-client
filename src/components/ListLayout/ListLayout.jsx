import React from 'react';
import styles from './ListLayout.module.css';

export const ListLayout = ({ children }) => {
    return(
        <div className={`${styles.grid}`}>{children}</div>
    )
}