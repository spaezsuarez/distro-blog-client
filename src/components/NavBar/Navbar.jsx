import React from "react";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink,useNavigate } from 'react-router-dom';
import NavDropdown from 'react-bootstrap/NavDropdown';

export const NavBar = () => {
  const navigate = useNavigate();

  const closeSession = () => {
    localStorage.removeItem('token');
    navigate('/login');
  }

  return (
    <Navbar bg="primary" variant="dark">
      <Navbar.Brand as={NavLink} to="/">Distro Blog</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse>
        <Nav className="mr-auto">
          <Nav.Link as={NavLink} to="/distributions">Distros</Nav.Link>
          <Nav.Link as={NavLink} to="/reviews">Reseñas de usuarios</Nav.Link>
        </Nav>
      </Navbar.Collapse>
      <NavDropdown title="User" variant="dark" id="nav-dropdown">
          <NavDropdown.Item eventKey="4.1" as={NavLink} to="/login">Iniciar Sesión</NavDropdown.Item>
          <NavDropdown.Item eventKey="4.1" as={NavLink} to="/sign-up">Registarme</NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item eventKey="4.2" onClick={closeSession}>Cerrar Sesión</NavDropdown.Item>
        </NavDropdown>
    </Navbar>
  )

}