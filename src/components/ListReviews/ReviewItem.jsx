import React from "react";
import { useNavigate } from 'react-router-dom';
import ListItemButton from '@mui/material/ListItemButton';
import Figure from 'react-bootstrap/Figure';
import styles from './ReviewItem.module.css';

export const ReviewItem = ({ id_distribution,name, logo }) => {
    const navigate = useNavigate();

    const viewDetail = () => {
        navigate(`/review/${id_distribution}`);
    }

    return (
        <ListItemButton className={styles.item_element} onClick={viewDetail}>
            <Figure className={styles.figure}>
                <Figure.Image
                    width={100}
                    height={100}
                    alt={name}
                    src={logo}
                />
            </Figure>
            &nbsp;
            &nbsp;
            &nbsp;
            <h4>{name}</h4>
        </ListItemButton>
    )
}