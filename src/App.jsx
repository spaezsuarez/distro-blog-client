import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import 'bootswatch/dist/flatly/bootstrap.min.css';
import './styles.css';

import { Layout } from './components/Layout/Layout';
import { LandingPage } from './pages/Landing/LandingPage';
import { Login } from './pages/Login/Login';
import { SignUp } from './pages/SignUp/SignUp';
import { Distributions } from './pages/Distributions/Distributions';
import { DetailDistribution } from './pages/DetailDistribution/DetailDistribution';
import { Reviews } from './pages/Reviews/Reviews';
import { ReviewDetail } from './pages/ReviewDetail/ReviewDetail';

export const App = () => {
    return (
        <Router>
            <Layout>
                <Routes>
                    <Route exact path="/" element={<LandingPage />} />
                    <Route exact path='/login' element={<Login />} />
                    <Route exact path='/sign-up' element={<SignUp />} />
                    <Route exact path='/distributions' element={<Distributions />} />
                    <Route exact path='/reviews' element={<Reviews />} />
                    <Route path='/detail/:id_distribution' element={<DetailDistribution />} />
                    <Route path='/review/:id_distribution' element={<ReviewDetail />} />
                </Routes>
            </Layout>
        </Router>
    )
}