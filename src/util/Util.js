import Swal from 'sweetalert2';

export const displayAlert = async (title,message,icon) => {
    await Swal.fire(
        title,
        message,
        icon
    );
}