export class HttpClient{

    constructor(url = 'http://localhost:5000') {
        this.url = url;
        this.headers = { 'Content-Type': 'application/json' };
    }

    addHeader(headerName,value){
      this.headers[headerName] = value;
    }

    async sendRequest(request) {
        return await fetch(request.url, {
          method: request.method,
          headers: request.headers,
          body: JSON.stringify(request.data),
        });
      }
    
    async get(endpoint) {
        return this.sendRequest({
          url: `${this.url}${endpoint}`,
          method: 'GET',
          headers:this.headers
        });
      }
    
    async post(endpoint, data) {
        return this.sendRequest({
          url: `${this.url}${endpoint}`,
          method: 'POST',
          headers: this.headers,
          data,
        });
      }
    
    async put(endpoint, data) {
        return this.sendRequest({
          url: `${this.url}${endpoint}`,
          method: 'PUT',
          headers: this.headers,
          data,
        });
      }
    
    async del(endpoint) {
        return this.sendRequest({
          url: `${this.url}${endpoint}`,
          method: 'DELETE',
        });
      }
}