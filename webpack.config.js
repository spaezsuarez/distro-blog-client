const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyWebPackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { resolve } = require('path');

module.exports = {

    entry:["regenerator-runtime/runtime.js",resolve(__dirname,'src/index.js')],

    output:{
        path:resolve(__dirname,'dist'),
        filename:'[fullhash].js',
        publicPath:'/',
        chunkFilename: '[id].[chunkhash].js'
    },

    resolve:{
        extensions:['.js','.jsx']
    },

    module:{
        rules:[
            {
                test:/\.js|[x]$/,
                exclude:/node_modules/,
                use:{
                    loader:'babel-loader',
                    options:{
                        presets:['@babel/preset-env','@babel/preset-react']
                    }
                }
            },
            {
                test: /\.s[ac]ss$/,
                use:["style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.css$/,
                use:["style-loader", "css-loader"]
            },
            {
                test: /\.(svg|png|jpe?g|gif)$/i,
                loader: "file-loader",
            },
        ]
    },

    plugins:[
        new CleanWebpackPlugin(),
        new HtmlWebPackPlugin({ template:resolve(__dirname,'public/index.html') }),
        new CopyWebPackPlugin({ patterns:[{from:resolve(__dirname,'public'),to:resolve(__dirname,'dist/public')}] })
    ],

    devServer:{
        host:'127.0.0.1',
        port:3000,
        hot:true,
        historyApiFallback:true
    }
};